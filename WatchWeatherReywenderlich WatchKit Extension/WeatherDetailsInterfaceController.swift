//
//  WeatherDetailsInterfaceController.swift
//  WatchWeatherReywenderlich WatchKit Extension
//
//  Created by Poonam Pandey on 26/01/19.
//  Copyright © 2019 Poonam Pandey. All rights reserved.
//

import WatchKit
import Foundation


class WeatherDetailsInterfaceController: WKInterfaceController {
    @IBOutlet var intervalLabel: WKInterfaceLabel!
    @IBOutlet var temperatureLabel: WKInterfaceLabel!
    @IBOutlet var conditionImage: WKInterfaceImage!
    @IBOutlet var conditionLabel: WKInterfaceLabel!
    @IBOutlet var feelsLikeLabel: WKInterfaceLabel!
    @IBOutlet var windLabel: WKInterfaceLabel!
    @IBOutlet var highTemperatureLabel: WKInterfaceLabel!
    @IBOutlet var lowTemperatureLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        guard let contextToPass = context as? NSDictionary, let datasource =  contextToPass["datasource"] as? WeatherDataSource else {
           return
        }
        
        if let index = contextToPass["longTermIndex"] as? Int  {
            let weather = datasource.longTermWeather[index]
            setTitle(weather.intervalString)
            intervalLabel.setHidden(true)
            updateTheWeatherDetails(weather)
        }
        
        if let index = contextToPass["shortTermIndex"] as? Int  {
            let weather = datasource.shortTermWeather[index]
            intervalLabel.setText(weather.intervalString)
            updateTheWeatherDetails(weather)
        }
        
        if let isThisActivePage = contextToPass["isThisActivePage"] as? Bool, isThisActivePage {
            becomeCurrentPage()
        }
    }
    
    func updateTheWeatherDetails(_ weather: WeatherData) {
        temperatureLabel.setText(weather.temperatureString)
        conditionImage.setImageNamed(weather.weatherConditionString)
        conditionLabel.setText(weather.weatherConditionString)
        feelsLikeLabel.setText(weather.feelTemperatureString)
        windLabel.setText(weather.windString)
        highTemperatureLabel.setText("\(weather.highTemperature)")
        lowTemperatureLabel.setText("\(weather.lowTemperature)")
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
