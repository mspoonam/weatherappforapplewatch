//
//  InterfaceController.swift
//  WatchWeatherReywenderlich WatchKit Extension
//
//  Created by Poonam Pandey on 24/01/19.
//  Copyright © 2019 Poonam Pandey. All rights reserved.
//

import WatchKit
import Foundation
import UIKit


class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var conditionsImage: WKInterfaceImage!
    @IBOutlet weak var windSpeedLabel: WKInterfaceLabel!
    @IBOutlet weak var feelsLikeLabel: WKInterfaceLabel!
    @IBOutlet weak var conditionsLabel: WKInterfaceLabel!
    @IBOutlet weak var temperatureLabel: WKInterfaceLabel!
    
    @IBOutlet weak var weatherTable: WKInterfaceTable!
    
    @IBOutlet var shortTermForecastLabel1: WKInterfaceLabel!
    @IBOutlet var shortTermForecastLabel2: WKInterfaceLabel!
    @IBOutlet var shortTermForecastLabel3: WKInterfaceLabel!
    
    @IBOutlet weak var shortTermForecastGroup: WKInterfaceGroup!
    
    @IBOutlet weak var loadingImage: WKInterfaceImage!
    
    @IBOutlet weak var containerGroup: WKInterfaceGroup!
    lazy var datasource: WeatherDataSource = {
        if var defaultType = UserDefaults.standard.string(forKey: "MeasurementMetric") {
            if defaultType == "Metric" {
                return WeatherDataSource(measurementSystem: .Metric)
            }
            return WeatherDataSource(measurementSystem: .USCustomary)
        }
        
        return WeatherDataSource(measurementSystem: .USCustomary)
    }()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        loadingImage.startAnimating()
        
        // Configure interface objects here.

        containerGroup.setHidden(true)
        delayAnimation(timeInSeconds: 1.5) {
            self.containerGroup.setHidden(false)
            self.loadingImage.setAlpha(0)
            self.loadingImage.setHeight(0)
            self.updateAllData()
        }
        
    }
    
    func delayAnimation(timeInSeconds: Double, closure: @escaping ()->()) {
        let afterTheseSeconds = DispatchTime.now() + timeInSeconds
        DispatchQueue.main.asyncAfter(deadline: afterTheseSeconds, execute: closure)
    }
    
    func updateAllData() {
        updateCurrentForecast()
        updateShortTermForecast()
        updateLongTermForecast()
        drawGraphInsideShortTermForecastGroup()
    }
    
    @IBAction func switchToMetric() {
        datasource = WeatherDataSource(measurementSystem: .Metric)
        updateAllData()
        saveMetricType(value: "Metric")
    }
    
    @IBAction func switchToUSCustomary() {
        datasource = WeatherDataSource(measurementSystem: .USCustomary)
        updateAllData()
        saveMetricType(value: "USCustomary")
    }
    
    func saveMetricType(value : String) {
        UserDefaults.standard.set(value, forKey: "MeasurementMetric")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func shortTermWeather1() {
        openShortTermWeatherDetailsAt(index: 0)
    }
    
    @IBAction func shortTermWeather2() {
        openShortTermWeatherDetailsAt(index: datasource.shortTermWeather.count/2)
    }
    
    @IBAction func shortTermWeather3() {
        openShortTermWeatherDetailsAt(index: datasource.shortTermWeather.count-1)
    }
    
    func getContextToPass(index: Int, isThisActivePage: Bool) -> NSDictionary {
        let contextToPass: NSDictionary = [
            "datasource" : datasource,
            "shortTermIndex" : index,
            "isThisActivePage": isThisActivePage
        ]
        return contextToPass
    }
    
    func openShortTermWeatherDetailsAt(index: Int) {
        presentController(withNamesAndContexts: [
            (name: "IDForWeatherDetials", context: getContextToPass(index: 0, isThisActivePage: (index == 0))),
            (name: "IDForWeatherDetials", context: getContextToPass(index: datasource.shortTermWeather.count/2, isThisActivePage: (index == datasource.shortTermWeather.count/2))),
            (name: "IDForWeatherDetials", context: getContextToPass(index: datasource.shortTermWeather.count-1, isThisActivePage: (index == datasource.shortTermWeather.count-1))),
            ])
    }
    
    func drawGraphInsideShortTermForecastGroup() {
//        let temperatures = datasource.shortTermWeather.map {
//            CGFloat($0.temperature)
//        }
        
        let temperatures = datasource.shortTermWeather.map {
            CGFloat($0.temperature)
        }
        
        let graphWidth: CGFloat = 300
        let graphHeight: CGFloat = 88
        
        UIGraphicsBeginImageContext(CGSize(width: graphWidth, height: graphHeight))
        let context = UIGraphicsGetCurrentContext()
        
        defer {
            UIGraphicsEndImageContext()
        }
        
        // drawing code
        let path = UIBezierPath()
        path.lineWidth = 4
        UIColor.green.withAlphaComponent(0.33).setStroke()
        
        // these line of code helped draw graph using some values
//        path.move(to: CGPoint(x: 0, y: temperatures[0]))
//        path.addLine(to: CGPoint(x: CGFloat(graphWidth/2),
//                                 y: CGFloat(graphHeight)-CGFloat(temperatures[temperatures.count/2])))
//        path.addLine(to: CGPoint(x: CGFloat(graphWidth),
//                                 y: CGFloat(graphHeight)-CGFloat(temperatures[temperatures.count-1])))
        // calculate and get real values
        
        guard let maxValue = temperatures.max(), let minValue = temperatures.min() else {
            return 
        }
        let temperatureSpeed = maxValue - minValue
        func xCoordinateForIndex(index: Int) -> CGFloat { return graphWidth * CGFloat(index) /
            CGFloat(temperatures.count - 1) }
        func yCoordinateForTemperature(temperature: CGFloat) -> CGFloat { return graphHeight - (graphHeight *
            (temperature - minValue) / temperatureSpeed) }
        path.move(to: CGPoint(x: 0, y: yCoordinateForTemperature(temperature: temperatures[0])))
        for (i,temperature) in temperatures.enumerated() {
            print("@printing x:\(i),y:\(temperature)")
            path.addLine(to: CGPoint(x: xCoordinateForIndex(index: i), y: yCoordinateForTemperature(temperature: temperature)))
        }
        path.stroke()
        // end drawing code
        
        if let cgBitmap = context!.makeImage() {
            let uiimage = UIImage.init(cgImage: cgBitmap)
            shortTermForecastGroup.setBackgroundImage(uiimage)
        }
        
    }
    
    func updateCurrentForecast(){
        let weather = datasource.currentWeather
        
        temperatureLabel.setText(weather.temperatureString)
        feelsLikeLabel.setText(weather.feelTemperatureString)
        windSpeedLabel.setText(weather.windString)
        conditionsLabel.setText(weather.weatherConditionString)
        conditionsImage.setImageNamed(weather.weatherConditionImageName)
        
    }
    
    func updateLongTermForecast() {
        weatherTable.setNumberOfRows(datasource.longTermWeather.count, withRowType: "LongTermForecastRow")
        
        for (index, weather) in datasource.longTermWeather.enumerated() {
            if let row =  weatherTable.rowController(at: index) as?  LongTermForecastRowController {
                row.conditionsLabel.setText(weather.weatherConditionString)
                row.conditionsImage.setImageNamed(weather.weatherConditionString)
                row.dateLabel.setText(weather.intervalString)
                row.temperatureLabel.setText(weather.temperatureString)
            }
        }
    }
    
    func updateShortTermForecast() {
        let labels = [shortTermForecastLabel1, shortTermForecastLabel2, shortTermForecastLabel3]
        let weatherData = [datasource.shortTermWeather[0], datasource.shortTermWeather[datasource.shortTermWeather.count/2], datasource.shortTermWeather[datasource.shortTermWeather.count-1]]
        
        for i in 0...2 {
            let label = labels[i]
            let weather = weatherData[i]
            label!.setText("\(weather.intervalString)\n\(weather.temperatureString)")
        }
    }

    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let contextToPass: NSDictionary = [
            "datasource" : datasource,
            "longTermIndex" : rowIndex
        ]
        pushController(withName: "IDForWeatherDetials", context: contextToPass)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
