//
//  LongTermForecastRowController.swift
//  WatchWeatherReywenderlich WatchKit Extension
//
//  Created by Poonam Pandey on 26/01/19.
//  Copyright © 2019 Poonam Pandey. All rights reserved.
//

import WatchKit

class LongTermForecastRowController: NSObject {

    @IBOutlet weak var dateLabel: WKInterfaceLabel!
    
    @IBOutlet weak var temperatureLabel: WKInterfaceLabel!
    
    @IBOutlet weak var conditionsLabel: WKInterfaceLabel!
    
    @IBOutlet weak var conditionsImage: WKInterfaceImage!
    
    
}
