//
//  GlanceInterfaceController.swift
//  WatchWeatherReywenderlich WatchKit Extension
//
//  Created by Poonam Pandey on 29/01/19.
//  Copyright © 2019 Poonam Pandey. All rights reserved.
//

import WatchKit
import Foundation


class GlanceInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
